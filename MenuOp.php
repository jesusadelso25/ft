<?php
require('php/ValidarSesion.php');
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=0.3">
    <title>Menu Admintracion</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/ListEvento.css">
    <link rel="stylesheet" href="css/Menu.css">
</head>
<body>
<header class="header">
    <div class="container logo-nav-container">
        <a href="Admint.html" class="logo">
                <img src="img/logo.png" width="50" id="img">
        </a>
        <nav class="navegacion">
            <ul class="menu">
                <li><a href="MenuOp.php">Inicio</a></li>
                <li><a>Gestión Usuario</a>
                    <ul class="submenu">
                        <li><a href="AgregarUsuario.php">Agregar</a></li>
                        <li><a href="ListaUsuario.php">Lista</a></li>
                    </ul></li></li>
                <li><a>Gestión Noticia</a>
                    <ul class="submenu">
                        <li><a href="AgregarEvento.php">Agregar</a></li>
                        <li><a href="ListaEvento.php">lista</a></li>
                    </ul>
                </li>
                <li><a href="php/CerrarSesion.php">Cerrar</a></li>
            </ul>
        </nav>
    </div>
</header>
    <div class="container-tabla_Evento">
        <div class="Titulo_E">Ultima Noticia agregada</a></div>
        <?php
            require('php/cn.php'); 
            $usuario="SELECT * FROM `evento` ORDER by fecha_Evento DESC";
            $resultado=mysqli_query($conexion,$usuario);
            $row=mysqli_fetch_assoc($resultado)?>
            <div class="Fecha_Event"><?php echo $row['fecha_Evento'];?></div>
            <div class="Titulo_Event"><?php echo $row['titulo'];?></a></div>
            <img class="img_Event" src="data:image/jpg;base64,<?php echo base64_encode($row['foto']);?>" width="500px">
            <p class="Inform_Event"><?php echo $row['informacion'];?></p> 
        <?php ;
            mysqli_free_result($resultado);
            mysqli_close($conexion);  
        ?>
    </div>  
    <footer class="finp">
    <di>
        <p>Bienvenid@: <?php echo $_SESSION['user']?></p>
    </di>
    </footer>
</body>
</html>