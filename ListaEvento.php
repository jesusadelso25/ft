<?php
require('php/ValidarSesion.php');
require('php/cn.php');
$usuario="SELECT * FROM `evento` ORDER by fecha_Evento DESC";
$resultado=mysqli_query($conexion,$usuario);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=0.3">
    <title>Document</title>
    <link rel="stylesheet" href="css/Menu.css">
    <link rel="stylesheet" href="css/tablas.css">
    <title>Lista de actividades o eventos para edita/Eliminar</title>
</head>
<body>
<header class="header">
    <div class="container logo-nav-container">
        <a href="Admint.html" class="logo">
                <img src="img/logo.png" width="50" id="img">
        </a>
        <nav class="navegacion">
            <ul class="menu">
                <li><a href="MenuOp.php">Inicio</a></li>
                <li><a>Gestión Usuario</a>
                    <ul class="submenu">
                        <li><a href="AgregarUsuario.php">Agregar</a></li>
                        <li><a href="ListaUsuario.php">Lista</a></li>
                    </ul></li></li>
                <li><a>Gestión Noticia</a>
                    <ul class="submenu">
                        <li><a href="AgregarEvento.php">Agregar</a></li>
                        <li><a href="ListaEvento.php">lista</a></li>
                    </ul>
                </li>
                <li><a href="php/CerrarSesion.php">Cerrar</a></li>
            </ul>
        </nav>
    </div>
</header>
<div class="top">.</div><br><br><br>
    <div class="container-tabla">
        <div class="tabla__titulo">Lista de Noticia</div>
        <div class="tabla__title">Titulo</div>
        <div class="tabla__title">Fecha </div>
        <div class="tabla__title">Foto</div>
        <div class="tabla__title">Operación</div>
        <?php 
        while($row=mysqli_fetch_assoc($resultado)){?>
            <div class="tabla__item"><?php echo $row['titulo'];?></div>
            <div class="tabla__item"><?php echo $row['fecha_Evento'];?></div>
            <div class="tabla__item"><img  src="data:image/jpg;base64,<?php echo base64_encode($row['foto']);?>" width="49px"></div>
            <div name="" class="tabla__item">
            <a class="n" href="ActualizarEvento.php?id=<?php echo $row["id"];?>"class="tabla__item__link">Editar</a>|
            <a class="n" href="php/EliminarEvento.php?id=<?php echo $row["id"];?>"class="tabla__item__link" class="tabla__item__link">Eliminar</a>
        </div>  
        <?php };
            mysqli_free_result($resultado);
            mysqli_close($conexion);
        ?>
        
    </div>
</body>
</html>